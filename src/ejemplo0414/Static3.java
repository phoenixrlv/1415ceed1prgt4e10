/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0414;

/**
 * Fichero: Static3.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Static3 {

  public int dato;

  public void metodo() { // Metodo no static
    dato++;
  }

  public static void main(String[] args) {
    // metodo(); // error
  }
}
