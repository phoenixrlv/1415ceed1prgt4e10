/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0427;

/**
 * Fichero: Inicializador.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Inicializador {

  private static int a;
  private final static int b;

  static {
    System.out.println("Inicializador 1");
  }

  static {
    a = 0;
    b = 0;
    System.out.println("Inicializador 2");
  }

  static {
    int a;
    try {
      a = 3 / 0;
    } catch (Exception e) {
      System.out.println("Inicializador 3");
    }
  }

  Inicializador() {
    System.out.println("Constructor");
  }

  public static void main(String[] args) {
    Inicializador a = new Inicializador();
  }
}
/* EJECUCION:
 Inicializador 1
 Inicializador 2
 Inicializador 3
 Constructor
 */
