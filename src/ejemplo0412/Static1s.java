/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0412;

/**
 * Fichero: Static1s.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Static1s {

  public static int dato = 0; // Variable static

  public static void metodo() {
    dato++;   // Quitamos el this
  }

  public static void main(String[] args) {
    metodo();
    System.out.println(dato);
  }
}
