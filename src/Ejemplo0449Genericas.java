
/**
 * Fichero: Ejemplo0617.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */

class ClaseGenerica<T> {

  T obj;

  public ClaseGenerica(T o) {
    obj = o;
  }

  public void classTipo() {
    System.out.println("El tipo de T es " + obj.getClass().getName());
  }
}

public class Ejemplo0449Genericas {

  public static void main(String[] args) {
    ClaseGenerica<Integer> intObj = new ClaseGenerica<Integer>(88);
    intObj.classTipo();
  }
}
/* Ejecucion:
 El tipo de T es java.lang.Integer
 */
