/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0408;

/**
 * Fichero: CopiaClase.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */
public class CopiaClase {

  private int a;

  CopiaClase(int i) {
    this.a = i;
  }

  CopiaClase(CopiaClase x) {
    this.a = x.a;
  }

  public static void main(String[] args) {
    CopiaClase x = new CopiaClase(1);
    CopiaClase y = new CopiaClase(x);
    System.out.println(y.a);
  }

}
/* EJECUCION:
1
*/
