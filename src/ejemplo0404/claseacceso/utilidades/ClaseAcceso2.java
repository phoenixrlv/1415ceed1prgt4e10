/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0404.claseacceso.utilidades;

/**
 * Fichero: ClaseAcceso2.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */

// Si quitamos public no puede accese clases del subpaquete
public class ClaseAcceso2 {

  /* Clase acceso1 debe ser public para que 
   * clasea acceso2 puede usarla.
   * */
  private int x = 1;

  public int getX() {
    return x;
  }

}
