/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0403;

/**
 * Fichero: Clase3.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 24-nov-2013
 */
// public class Clase1c  {  // No puede ser public
class Clase1c {

  private int x = 1;

  public int getX() {
    return x;
  }
}

public class Clase3 {

  public static void main(String[] args) {

      // Clase1 c2= new Clase1(); // Bien si Clase1 es publica
    Clase1c c1 = new Clase1c();
    System.out.println(c1.getX());

      // Clase1 c2= new Clase1(); // Error sino es public
  }
}
