/**
 * Fichero: Ejemplo0611.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Ejemplo0442PersonaSobrecarga {

  private int sinsegundo = 0;
  private String nombre;
  private String apellido1;
  private String apellido2;

  public void setNombre(String nom, String ape1, String ape2) {
    nombre = nom;
    apellido1 = ape1;
    apellido2 = ape2;
  }

  public void setNombre(String nom, String ape1) {
    nombre = nom;
    apellido1 = ape1;
    sinsegundo = 1;
  }

  public int setNombre() {
    return sinsegundo;
  }

  public static void main(String args[]) {
    Ejemplo0442PersonaSobrecarga p1 = new Ejemplo0442PersonaSobrecarga();
    Ejemplo0442PersonaSobrecarga p2 = new Ejemplo0442PersonaSobrecarga();
    p1.setNombre("Paco", "Aldarias", "Raya");
    p2.setNombre("Paco", "Aldarias");
    System.out.println(p1.nombre + " " + p1.apellido1 + " " + p1.apellido2);
    System.out.println(p2.nombre + " " + p2.apellido1);
  }
}
/* EJECUCION:
 Paco Aldarias Raya
 Paco Aldarias 
 */
