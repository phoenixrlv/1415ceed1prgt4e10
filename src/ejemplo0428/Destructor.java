/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0428;

/**
 * Fichero: Destructor.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Destructor {

  private int ancho = 0;
  private int alto = 0;

  Destructor(int a, int b) {
    ancho = a;
    alto = b;
  }

  protected void finalize() {
    System.out.println("Adios");
  }

  public static void main(String[] args) {
    for (int i = 0; i < 20; i++) {
      Rectangulo r = new Rectangulo(5, 5);
    }
    System.runFinalization();
    System.gc();

  }

}
