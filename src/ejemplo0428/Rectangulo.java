/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0428;

/**
 * Fichero: rectangulo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class Rectangulo {

  private int ancho;
  private int alto;

  Rectangulo(int an, int al) {
    this.ancho = an;
    this.alto = al;
  }

  public int area() {
    return ancho * alto;
  }
}
