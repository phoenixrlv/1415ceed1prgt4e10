/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0418;

import ejemplo0418a.Objeto;

/**
 * Fichero: ParametroReferencia1.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class ParametroReferencia1 {

  public static void main(String[] args) {
    Objeto v = new Objeto();
    v.a = 2;
    v.funcion(v);
    System.out.println(v.a);
  }
}
/*EJECUCION:
 1
 */
