/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0418;

/**
 * Fichero: ParametroReferencia.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class ParametroReferencia {

  public static void funcion(int[] y) {
    y[0] = 5;
  }

  public static void main(String[] args) {
    int[] x = {3};
    funcion(x);
    System.out.println(x[0]);
  }
}
/*EJECUCION:
5
*/
