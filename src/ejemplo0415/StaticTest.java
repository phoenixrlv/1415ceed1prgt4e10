/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0415;

/**
 * Fichero: StaticTest.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
/**
 * Los metodos static de una clase no pueden acceder a miembros de instancia.
 */
public class StaticTest {

  public int dato;

  public static int datostatico = 0;

  public void Test() {
    dato = -1;
  }

  public void metodo() {
    this.datostatico++;
  }

  public static void metodostatico() {
    // Error 1: Metodo static no puede usar this.
    // this.datostatico++;
    datostatico++;
  }

  public int metodoMath() {
    return Math.abs(dato);
  }

  public static void main(String[] args) {
    StaticTest a = new StaticTest();
    // Error 2: Metodo static no accede a miembros no estatic
    // dato++;
    datostatico++;
    metodostatico();
    // Error 3: Metodo static no puede acceder a metodos no static.
    // metodo();
    System.out.println(Math.random());
    // La clase Math tiene metodos static, que se pueden
    // llamar desde metodos static.
    System.out.println(a.metodoMath());
  }
}
