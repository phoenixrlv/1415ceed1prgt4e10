/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0438PolimorfismoPersona;

/**
 * Fichero: Empleado.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Empleado extends Persona {

  protected int sueldoBase;

  public void setSueldoBase(int s) {
    sueldoBase = s;
  }

  public int getSueldo() {
    return sueldoBase;
  }
}
