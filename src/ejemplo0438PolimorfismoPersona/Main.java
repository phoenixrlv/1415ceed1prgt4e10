/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0438PolimorfismoPersona;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Main {

  public static void main(String args[]) {
    Persona p1 = new Empleado();
    Empleado e1 = new Encargado();

    //Persona-Empleado
    p1.setNombre("Paco Aldarias");
    //p1.setSueldoBase(100); // Error
    ((Empleado) p1).setSueldoBase(100); // Corregido
    // System.out.println(p1.getSueldo()); Error
    // Empleado  e = new Persona(); // Error.
    
    // Empleado-Encargado
    e1.setSueldoBase(500);
    // e1.setPuesto("Jefa Almacen"); //Error
    ((Encargado) e1).setPuesto("Jefa Almacen"); // Corregido
    System.out.println(e1.getSueldo());
  }
}
/* EJECUCION:
 550
 */
