/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0438PolimorfismoPersona;

/**
 * Fichero: Persona.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 13-dic-2013
 */
public class Persona {

  private String nombre;
  private String puesto;

  public void setNombre(String nom) {
    nombre = nom;
  }

  public String getNombre() {
    return nombre;
  }

  public void setPuesto(String p) {
    puesto = p;
  }
}
