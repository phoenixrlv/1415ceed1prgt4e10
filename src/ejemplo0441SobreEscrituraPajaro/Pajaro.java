/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0441SobreEscrituraPajaro;

/**
 * Fichero: Pajaro.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 16-dic-2013
 */
public class Pajaro {

  protected String nombre;
  protected String color;

  public Pajaro() {
    nombre = null;
    color = null;
  }

  public void setNombre(String n) {
    nombre = n;
  }

  public void setColor(String c) {
    color = c;
  }

  public String getDetalles() {
    return "Nombre: " + nombre + "\n" + "Color: " + color + "\n";
  }
}
