package ejemplo0430Herencia;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-nov-2013
 */
public class Main {

  public static void main(String args[]) {
    Rectangulo r = new Rectangulo(2, 3);
    r.setColor("verde");
    r.mostrar();

    Cuadrado c = new Cuadrado(3);
    c.setColor("rojo");
    c.mostrar();

    //System.out.println("Cuadrado Color: "+c.color+" Lado: "+c.lado);
    System.out.println("Cuadro Main. Color: " + c.getColor() + " Lado: " + c.getLado());

  }
}
/* EJECUCION:
 Rectangulo: Color: verde Base: 2 Altura: 3
 Cuadrado: Color: rojo Lado: 3
 Cuadro Main. Color: rojo Lado: 3
 */
