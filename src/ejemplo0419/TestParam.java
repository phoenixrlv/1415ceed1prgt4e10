/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0419;

/**
 * Fichero: TestParam.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class TestParam {

  public static void cambiar(int x) {
    x++;
  }

  public static void cambiar2(int[] par) {
    par[0]++;
  }

  public static void main(String[] args) {
    int x = 3;
    int[] array = {3};
    cambiar(x);
    System.out.println(x);
    cambiar2(array);
    System.out.println(array[0]);
  }

}
/* EJECUCION:
 3
 4
 */
