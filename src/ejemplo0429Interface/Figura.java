package ejemplo0429Interface;

/**
 * Fichero: Figura.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-nov-2013
 */
public interface Figura {

  int area();
  // int perimetro(); // Error pq rectangulo no lo tiene
  // int volumen(); // Error pq no la tiene implementada ninguna clase

}
