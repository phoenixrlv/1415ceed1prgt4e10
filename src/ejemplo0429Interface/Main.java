package ejemplo0429Interface;

/**
 * Fichero: Main.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-nov-2013
 */
public class Main {

  public static void main(String[] args) {

    Figura f1 = new Rectangulo(2, 3);
    Rectangulo f2 = new Rectangulo(1, 2);
      // Figura r3=new Figura(4,1); // Error no se puede instanciar

    Figura f3 = new Cuadrado(2);

    System.out.println(f1.area());
    System.out.println(f2.area());
    System.out.println(f3.area());
  }

}
