package ejemplo0429Interface;

/**
 * Fichero: Rectangulo.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-nov-2013
 */
public class Rectangulo implements Figura {

  private int ancho;
  private int alto;

  Rectangulo(int an, int al) {
    this.ancho = an;
    this.alto = al;
  }

  public int area() {
    return ancho * alto;
  }
}
