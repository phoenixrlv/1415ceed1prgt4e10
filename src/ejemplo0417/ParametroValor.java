/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package ejemplo0417;

/**
 * Fichero: ParametroValor.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 26-nov-2013
 */
public class ParametroValor {

  public static void funcion(int y) {
    // y es el parametro de la funcion
    y = 2;
  }

  public static void main(String[] args) {
    int x = 1;
    funcion(x);
    System.out.println(x); // x no cambia
  }
}

/*EJECUCION:
 1
 */
